# Autolayout


1. The Cassowary Linear Arithmetic Constraint Solving Algorithm

2. How it works on iOS?

3. Demo

4. Resources

___

### 1. The Cassowary Linear Arithmetic Constraint Solving Algorithm
----

### History

- In 1963, [Ivan Sutherland (MIT)][Scatchpad] is the first one to use constraints in user interfaces and interactive systems.

- In 1997 [Alan Borning, Kim Marriott, Peter Stuckey, and Yi Xiao][Cassowary First Paper], describes the original version of Cassowary.

- In 2001 [Alan Borning, Greg J. Badros and Peter J. Stuckey][Cassowary Second Paper] publishes a more polished version of it.

### Definition
- Linear equality and inequality constraints arise naturally in specifying many aspects of user interfaces especially layout and other geometric relations.

- Cassowary developed a rule-based system that enabled developers to describe relationships between views. These relationships were described through constraints. Constraints are rules that describe how one view’s layout is limited with respect to another.

- It offers an automatic solver that transforms its system of constraint-based layout rules (essentially a set of simultaneous linear equations) into view geometries that express those rules.

- Since its debut, Cassowary has been ported to JavaScript, .NET/Java, Python, Smalltalk, C++, and, via AutoLayout, to Cocoa and Cocoa Touch.

### 2. How it works on iOS?
---

> `AutoLayout` is essentially a linear equation solver that attempts to find a satisfiable geometric expression of its rules.

> A `constraint` must be `satisfiable` and `sufficient`.
    
##### Satisfiability

- A constraint is satisfiable when it can be created in a valid manner. A view cannot be both to the left `and` to the right of another view.

- What can go wrong?

```
Unable to simultaneously satisfy constraints.
Probably at least one of the constraints in the following list is one you
don't want. Try this: (1) look at each constraint and try to figure out which
you don't expect; (2) find the code that added the unwanted constraint or
constraints and fix it.
(Note: If you're seeing NSAutoresizingMaskLayoutConstraints that you don't
understand, refer to the documentation for the UIView property
translatesAutoresizingMaskIntoConstraints)
(
    "<NSLayoutConstraint:0x7147d40 H:[TestView:0x7147c50(360)]>",
    "<NSLayoutConstraint:0x7147e70 H:[TestView:0x7147c50(140)]>"
)

Will attempt to recover by breaking constraint
    <NSLayoutConstraint:0x7147d40 H:[TestView:0x7147c50(360)]>

Break on objc_exception_throw to catch this in the debugger.
The methods in the UIConstraintBasedLayoutDebugging category on
    UIView listed in <UIKit/UIView.h> may also be helpful.
```

##### Sufficiency
- A constraint is sufficient when it has one, and one only, solution. Insufficient or ambiguous layout creates random results when faced with many possible layout solutions. (Does not mean `hard coded`)

- You might request that one view lies to the right of the other, but unless you tell the system otherwise, you might end up with the left view at the top of the screen and the right view at the bottom. That one rule doesn’t say anything about vertical orientation.

![Sufficiency][Sufficiency]

## Constraint Attributes

![Attributes]

- Left(A), right(B), top(C), and bottom(D)

- Leading(A) and trailing(B) (Use this for internationalization)

- Width(E) and height(F)

- CenterX (H) and CenterY(G)

- Baseline(I) (Typically a fixed offset above its bottom attribute)

#### Frames vs. Alignment Rectangles

- Frames describe where to place views on the screen and how big those views will be. When laying out views, constraints use a related geometric element called an alignment rectangle.

- The alignment rectangle is based on the presentation of the item’s content, Auto Layout uses the alignment rectangle instead of the item’s frame rectangle. By working with alignment rectangles instead of frames, Auto Layout ensures that key information like a view’s edges and center are properly considered during layout. Unlike frames, a view’s alignment rectangle should be limited to a core visual element. Its size should remain unaffected as new items are drawn onto the view.

![Alignment Rectangle]

#### Content Size Constraints

- To lay out elements in the screen, Autolayout must have two sets of constraints, namely:

- Origin (the location, expressed in `x` and `y` position)

- Size (how big it is, expressed in width and height values)

- Although you must provide these four informations, some elements have what's called `intrinsic content size`.

#### Intrinsic Content Size

- Labels, images and controls, like buttons, often depend on the content they present. In other other they "know" how to calculate it's size.

- For such elements, only two constraints are sufficient for the layout engine to solve. It's left to you, then, to provide the `x` and `y` position.

#### Content Hugging and Compression Resistance

- Content hugging constraints restrict the amount of stretching and padding a view allows itself to experience. A high-content hugging priority matches a view’s frame to its intrinsic content size.

- Compression resistance constraints prevent a view from clipping its content. A high resistance priority ensures that a view’s intrinsic content is fully displayed.

![Hugging and Compression]

#### Constraint Math

> All constraints, regardless of how they are created, are essentially equations or inequalities with the following form:

- `y R m * x + b`

> `x` and `y`: size (width and height) or position (x and y)

> `R`: how the attributes compare to each other (==, >=, <=)

> `m`: is a constant scaling fator

> `b`: is a constant offset

- For example, you might say, View B’s left side should be placed 20 points to the right of View A’s right side. The relation equation that results is something like this:

- (View B left) = (View A right) + 20

- Autolayout also uses `priorities` to rank and honor constraints. They play an important role in adding nuance to a layout, with it you can allow the layout engine to decide which rules win out in a otherwise conflicting design.

- A range from 1-1000

|             Name            | Value |
|:---------------------------:|:-----:|
|   NSLayoutPriorityRequired  |  1000 |
| NSLayoutPriorityDefaultHigh |  750  |
|  NSLayoutPriorityDefaultLow |  250  |

___

### 3. Demo
___


### 4. Resources
---

- [Mysteries of Auto Layout, Part 1]

- [Mysteries of Auto Layout, Part 2]
 
- [Overconstrained]

> Pro tip: Use the [WWDC App for macOS]to watch WWDC videos.

<!-- Resource links -->
[Scatchpad]: https://www.youtube.com/watch?v=57wj8diYpgY
[Cassowary First Paper]: http://constraints.cs.washington.edu/solvers/uist97.pdf
[Cassowary Second Paper]: https://constraints.cs.washington.edu/solvers/cassowary-tochi.pdf
[Mysteries of Auto Layout, Part 1]: https://developer.apple.com/videos/play/wwdc2015/218/
[Mysteries of Auto Layout, Part 2]: https://developer.apple.com/videos/play/wwdc2015/219/
[Overconstrained]: http://overconstrained.io/
[WWDC App for macOS]: https://github.com/insidegui/WWDC
<!-- Images -->
[Sufficiency]: images/sufficiency.png
[Attributes]: images/attributes.png
[Alignment Rectangle]: images/alignment_rectangle.png
[Hugging and Compression]: images/hugging_compression.png